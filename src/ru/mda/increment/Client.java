package ru.mda.increment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;

public class Client {

    public static void main(String[] args) {
        try(Socket socket = new Socket("localhost", 6666);
            InputStream inputStream = socket.getInputStream();
            OutputStream outputStream = socket.getOutputStream()){
            int number = 1;
            outputStream.write(number);
            System.out.println("Отправленно: " + number);

            while ((number = inputStream.read()) != -1){
                System.out.println("Пришло: " + number);
                if(number > 25){
                    break;
                }
                outputStream.write(number++);
                System.out.println("Отправленно: " + number);
                outputStream.flush();
            }
        }catch (IOException e){
            e.printStackTrace();
        }
    }
}
