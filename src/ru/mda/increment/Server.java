package ru.mda.increment;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class Server {
    public static void main(String[] args) throws IOException{
        ServerSocket serverSocket = new ServerSocket(6666);
        System.out.println("Сервер ждёт");

        try(Socket clientSocket = serverSocket.accept();
        InputStream inputStream = clientSocket.getInputStream();
        OutputStream outputStream = clientSocket.getOutputStream()){

            System.out.println("Новое соединение:" + clientSocket.getInetAddress().toString());
            int number;
            while ((number = inputStream.read()) != -1){
                System.out.println("Пришло от клиента: " + number);
                outputStream.write(++number);
                System.out.println("Отправленно клиенту: " + number);
                outputStream.flush();
            }
            System.out.println("Клиент отключился");
        }
    }
}
